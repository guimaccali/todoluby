const taskLocalStorageVariable = 'Tasks'

const TaskHelper = {
  getTasks: () => {
    const task = JSON.parse(TaskHelper.getRecords())

    if (task) {
      return JSON.parse(TaskHelper.getRecords()).reverse()
    } else {
      return []
    }
  },

  getRecords: () => {
    return localStorage.getItem(`${taskLocalStorageVariable}`)
  },

  setRecords: (records: Array<TaskFace>) => {
    localStorage.setItem(`${taskLocalStorageVariable}`, JSON.stringify(records))
  },

  record: (task: TaskFace) => {
    if (!task.dateTime) {
      task.dateTime = new Date().toISOString()
    }

    let records = TaskHelper.getRecords()

    if (!records) {
      TaskHelper.setRecords([])
      records = TaskHelper.getRecords()
    }

    const recordsJson = JSON.parse(records)
    recordsJson.push(task)
    TaskHelper.setRecords(recordsJson)
  }
}

export default TaskHelper
