import * as yup from 'yup'

const YupHelper = {
  errorTreatment: (error: { inner: yup.ValidationError[] }) => {
    type ErrorsFace = {
      field: string
      message: string
    }

    const errors: ErrorsFace[] = []

    error.inner.forEach((itemError: yup.ValidationError) => {
      console.log(itemError)
      const nodeError = {
        field: itemError.path,
        message: itemError.message
      }
      errors.push(nodeError)
    })

    return errors
  }
}

export default YupHelper
