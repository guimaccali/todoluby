import styled from 'styled-components'

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #001b2e;
  border-radius: 25px;
  padding: 23px;
  color: #fff;
  margin-bottom: 30px;
  transition: all 0.3s;

  &:hover {
    transform: scale(1.02);
  }

  & p {
    margin: 0;
    margin-bottom: 20px;
    height: 60px;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3; /* number of lines to show */
    -webkit-box-orient: vertical;
    text-align: justify;
  }

  & span {
    font-weight: bold;
    font-size: 12px;
  }
`
