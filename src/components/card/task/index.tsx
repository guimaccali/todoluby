import React from 'react'

import DateHelper from '../../../helpers/DateHelper'

import { Card } from './styles'

function TaskCard({ description, dateTime }: TaskFace) {
  return (
    <>
      <Card>
        <p>{description}</p>
        <span>{DateHelper.getDateTimeFormat(dateTime)}</span>
      </Card>
    </>
  )
}

export default TaskCard
