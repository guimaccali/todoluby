import styled from 'styled-components'

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  & img {
    width: 150px;
    height: 143px;
    margin-bottom: 20px;
  }
`

export const Title = styled.h2`
  margin-top: 20px;
  margin-bottom: 30px;
  text-align: center;
  color: white;
`
