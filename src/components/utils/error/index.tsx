import React from 'react'
import { AiOutlineReload } from 'react-icons/ai'
import Clickable from '../clickable'
import { Card, Title } from './styles'

type ErrorFace = {
  message: string
  reload?: () => void
  noImg?: boolean
}

function Error({ message, reload, noImg }: ErrorFace) {
  return (
    <>
      <div className="container-fluid">
        <div className="container ped-lr">
          <div className="row">
            <div className="col-12">
              <Card>
                <Title>{message}</Title>
                {noImg ? '' : <img src="/imgs/Logo.png" alt="Imagem de Erro" />}
                {reload ? (
                  <Clickable title="Try Again" action={() => reload}>
                    <AiOutlineReload />
                    <span>Reload</span>
                  </Clickable>
                ) : (
                  ''
                )}
              </Card>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Error
