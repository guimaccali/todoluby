import React, { ReactNode } from 'react'
import Fade from 'react-reveal/Fade'

import Head from 'next/head'

import { Card, ContText, Text } from './styles'

type ErrorPageFace = {
  statusCode: number
  title: string
  message: string
  children: ReactNode
}

function ErrorPage({ statusCode, message, title, children }: ErrorPageFace) {
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <main>
        <div className="container-fluid">
          <div className="row">
            <Fade right>
              <Card>
                <ContText>
                  <Text>
                    <h1>{statusCode}</h1>
                    <h2>{message}</h2>
                    <div>{children}</div>
                  </Text>
                </ContText>
              </Card>
            </Fade>
          </div>
        </div>
      </main>
    </>
  )
}

export default ErrorPage
