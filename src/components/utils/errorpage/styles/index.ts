import styled from 'styled-components'

export const Card = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  height: calc(100vh - 65px);
`

export const ContText = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  align-self: center;
  background-color: rgba(240, 248, 255, 0.42);
  width: 100%;
`

export const Text = styled.p`
  width: 300px;
  padding: 20px;

  & > * {
    margin-bottom: 20px;
  }

  & p,
  & h1,
  & h2 {
    color: black;
  }

  & p {
    font-size: 16px;
  }

  & h1 {
    font-size: 45px;
  }

  & h2 {
    font-size: 25px;
  }
`
