import React from 'react'
import NumberFormat from 'react-number-format'
import TextareaAutosize from 'react-textarea-autosize'
import Inline from '../inline'

import { Node } from './styles'

type InputFace = {
  type: 'email' | 'password' | 'text' | 'textarea' | 'number' | 'url'
  name: string
  value: any
  title?: string
  placeholder?: string
  wrongMessage?: string
  onChange: React.ChangeEventHandler<HTMLInputElement & HTMLTextAreaElement>
}

function Input({
  type,
  name,
  title,
  value,
  placeholder,
  wrongMessage,
  onChange
}: InputFace) {
  if (type === 'textarea') {
    return (
      <>
        <Node>
          {title ? <label htmlFor={`${name}`}>{title}</label> : ''}

          <TextareaAutosize
            // type="text"
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            name={name}
          />
          {wrongMessage ? (
            <div>
              <Inline type="error" message={wrongMessage} />
            </div>
          ) : (
            ''
          )}
        </Node>
      </>
    )
  }

  if (type === 'number') {
    return (
      <>
        <Node>
          {title ? <label htmlFor={`${name}`}>{title}</label> : ''}
          <NumberFormat
            type="text"
            value={value}
            onChange={onChange}
            decimalSeparator={false}
          />
          {wrongMessage ? (
            <div>
              <Inline type="error" message={wrongMessage} />
            </div>
          ) : (
            ''
          )}
        </Node>
      </>
    )
  }

  return (
    <>
      <Node>
        {title ? <label htmlFor={`${name}`}>{title}</label> : ''}
        <input
          value={value}
          onChange={onChange}
          placeholder={placeholder}
          type={type}
          name={name}
        />
        {wrongMessage ? (
          <div>
            <Inline type="error" message={wrongMessage} />
          </div>
        ) : (
          ''
        )}
      </Node>
    </>
  )
}

export default Input
