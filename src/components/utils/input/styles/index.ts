import styled from 'styled-components'

export const Node = styled.div`
  display: flex;
  align-items: initial;
  flex-direction: column;
  transition: all 0.2s;
  width: 100%;

  & label {
    font-size: 14px;
    color: white;
    font-weight: bold;
    margin-bottom: 7px;
  }

  & input,
  & textarea {
    border-radius: 25px;
    color: black;
    font-size: 16px;
    background-color: #adb6c4;
    border: solid 1px #294c60;
    transition: all 0.2s;
    outline: none;
  }

  & input {
    height: 43px;
    padding-left: 15px;
    padding-right: 15px;
  }

  & textarea {
    height: auto;
    min-height: 60px;
    padding: 15px;
    resize: vertical;
  }

  & input:hover,
  & textarea:hover {
    outline: none;
    border-color: #0c9637 !important;
  }
`
