import React, { ReactNode, useEffect } from 'react'
import { AiOutlineClose } from 'react-icons/ai'
import {
  BgPrimary,
  ContAside,
  ContAsideFix,
  FixedCont,
  HeaderCont,
  ImgCont,
  Mt60
} from './styles'

import Clickable from '../clickable'

type ModalFace = {
  open: boolean
  setClose: (flag: boolean) => void
  children: ReactNode
}

function Modal({ open, setClose, children }: ModalFace) {
  function bodyControl(flag: boolean) {
    const { body } = document
    if (flag) {
      body.classList.remove('scroll-off')
    } else {
      body.classList.add('scroll-off')
    }
  }

  function openModal() {
    bodyControl(false)
    document.getElementById('scroll').scrollTop = 0
  }

  function closeModal() {
    bodyControl(true)
  }

  useEffect(() => {
    if (open) {
      openModal()
    } else {
      closeModal()
    }
  }, [open])

  return (
    <>
      <ContAside
        id="scroll"
        className={open ? `contaside contasideactivate` : `contaside`}
      >
        <ContAsideFix>
          <FixedCont className="container-fluid">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <HeaderCont>
                    <ImgCont>
                      <img src="/imgs/logo.png" alt="Site Logo" />
                    </ImgCont>
                    <Clickable
                      title="Close Menu"
                      action={() => setClose(!open)}
                      noStyle
                    >
                      <AiOutlineClose />
                    </Clickable>
                  </HeaderCont>
                </div>
              </div>
            </div>
          </FixedCont>
          <div className="col-12">
            <Mt60>
              <BgPrimary>{children}</BgPrimary>
            </Mt60>
          </div>
        </ContAsideFix>
      </ContAside>
    </>
  )
}

export default Modal
