import styled from 'styled-components'

export const FixedCont = styled.div`
  position: fixed;
  width: 100%;
  background-color: #001b2e;
  z-index: 250;
`

export const Mt60 = styled.div`
  margin-top: 60px;
`
export const ContAside = styled.div`
  &.contaside {
    display: block;
    position: fixed;
    right: 0;
    top: 0;
    height: 100vh;
    background-color: #294c60;
    color: #fff;
    width: 0;
    transition: all 0.25s;
    border-left: none;
    overflow-y: auto;
    overflow-x: hidden;
    overscroll-behavior-y: contain;
    z-index: 250;
  }

  &.contaside::-webkit-scrollbar {
    width: 0px !important;
  }

  &.contasideactivate {
    width: 100vw !important;
  }
`
export const BgPrimary = styled.div`
  background-color: #294c60;
`
export const ContAsideFix = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #294c60;
  width: 100vw;
`
export const HeaderCont = styled.div`
  & {
    display: inline-flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 60px;
    padding-left: 15px;
    padding-right: 15px;
  }

  & p {
    display: flex;
    color: white;
    font-size: 20px;
    margin: 0;
  }

  & button {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 40px;
    width: 40px;
    border-radius: 50%;
    transition: all 0.3s;
    cursor: pointer;
  }

  & button:hover {
    transform: scale(1.1);
  }

  & button svg {
    width: 28px;
    height: 28px;
    color: #fff;
  }
`
export const ImgCont = styled.div`
  display: inline-flex;
  height: inherit;
  align-items: center;

  & img {
    height: 48px;
    border-radius: 50%;
  }
`
