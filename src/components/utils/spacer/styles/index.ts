import styled from 'styled-components'

export const Spacer = styled.div`
  height: ${(props: SpaceStyleProps) => `${props.height}px`};
  width: ${(props: SpaceStyleProps) => `${props.width}px`};
`
