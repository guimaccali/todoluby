import { Spacer } from './styles'

function TodoContent({ width, height }: SpaceStyleProps) {
  return (
    <>
      <Spacer height={height} width={width} />
    </>
  )
}

export default TodoContent
