import React, { useState, useEffect } from 'react'
import Fade from 'react-reveal/Fade'

// Icons import
import { FiMenu } from 'react-icons/fi'
import { AiFillInfoCircle } from 'react-icons/ai'
import { MdClose } from 'react-icons/md'

// Common Project libs
import Clickable from '../clickable'

import {
  CardMenu,
  Cont,
  ContAside,
  ContAsideFix,
  HeaderCont,
  Img,
  Menu,
  MenuItem,
  NavMenu
} from './styles'

function Nav() {
  const [menuActive, setMenuActive] = useState<boolean>(false)
  const [headerActive, setHeaderActive] = useState<boolean>(false)

  const [links] = useState([
    {
      icone: <AiFillInfoCircle />,
      nome: 'About',
      url: '/'
    }
  ])

  function setMenuOpenWithWithSize() {
    setMenuActive(window.innerWidth >= 1800)
    setHeaderActive(window.innerWidth <= 1800)
  }

  useEffect(() => {
    window.addEventListener('resize', function () {
      setMenuOpenWithWithSize()
    })
    setMenuOpenWithWithSize()
  }, [])

  return (
    <>
      <Cont>
        <div className="container">
          <NavMenu>
            <Menu>
              <li>
                <Fade left>
                  <Img>
                    <Clickable title="Home" href="/" noStyle>
                      <div>
                        <img src="/imgs/logo.png" alt="Site Logo" />
                      </div>
                    </Clickable>
                  </Img>
                </Fade>
              </li>
            </Menu>
            <Menu>
              <Clickable
                title="Open Menu"
                action={() => setMenuActive(!menuActive)}
                noStyle
              >
                <FiMenu />
                <p>Menu</p>
              </Clickable>
            </Menu>
          </NavMenu>
          <ContAside
            className={menuActive ? `contaside contasideactivate` : `contaside`}
          >
            <ContAsideFix>
              {headerActive ? (
                <HeaderCont>
                  <p>Menu</p>
                  <Clickable
                    title="Fechar Menu"
                    action={() => setMenuActive(!menuActive)}
                    noStyle
                  >
                    <MdClose />
                  </Clickable>
                </HeaderCont>
              ) : (
                ''
              )}
              <CardMenu>
                <div>
                  <img src="/icons/icon126.png" alt="Logo do site" />
                </div>
                <span>Guilherme Maccali</span>
              </CardMenu>
              <div>
                {Object.keys(links).map((key: any) => (
                  <Clickable
                    key={key}
                    title={links[key].nome}
                    href={`${links[key].url}`}
                    noStyle
                  >
                    <MenuItem>
                      <span>{links[key].icone}</span>
                      <p>{links[key].nome}</p>
                    </MenuItem>
                  </Clickable>
                ))}
              </div>
            </ContAsideFix>
          </ContAside>
        </div>
      </Cont>
    </>
  )
}

export default Nav
