import styled from 'styled-components'

export const Cont = styled.div`
  position: fixed;
  z-index: 100 !important;
  width: 100%;
  background-color: #001b2e;
`

export const Space = styled.div`
  height: 65px;
`

export const NavMenu = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: inherit;
  height: 65px;
`

export const Menu = styled.ul`
  display: flex;
  align-items: center;
  height: inherit;
  padding: 0;
  margin: 0;
  list-style: none;
  transition: all 0.2s;

  @media only screen and (max-width: 768px) {
    & div a p,
    & div button p {
      display: none;
    }
  }

  & div a,
  & div button {
    display: flex;
    /* background-color: #001B2E; */
    height: 65px;
    outline: none;
    justify-content: center;
    align-items: center;
    border: none;
    padding-left: 15px;
    padding-right: 15px;
    transition: all 0.2s;
  }

  & div a:hover,
  & div button:hover {
    height: 115px !important;
    border-radius: 0 0 30px 30px;
    background-color: #294c60;
  }

  & div a p,
  & div button p {
    padding-left: 10px;
    color: white;
  }

  & div a svg,
  & div button svg {
    width: 25px;
    height: 25px;
    color: #fff !important;
  }
`
export const Img = styled.div`
  display: inline-flex;
  height: inherit;
  align-items: center;

  & img {
    height: 55px;
    border-radius: 50%;
  }
`
export const ContAside = styled.div`
  &.contaside {
    display: block;
    position: fixed;
    right: 0;
    top: 0;
    height: 100vh;
    background-color: #001b2e;
    color: #fff;
    width: 0;
    overflow: auto;
    transition: all 0.2s;
    border-left: none;
    overflow: hidden;
  }
  &.contasideactivate {
    width: 300px !important;
  }
`

export const ContAsideFix = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
`
export const HeaderCont = styled.div`
  display: inline-flex;
  justify-content: space-between;
  align-items: center;
  height: 60px;
  padding-left: 15px;
  padding-right: 15px;

  & p {
    display: flex;
    color: white;
    font-size: 20px;
    margin: 0;
  }

  & button {
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;
    outline: none;
    height: 45px;
    width: 45px;
    border-radius: 50%;
    background-color: #001b2e;
    transition: all 0.2s;
    cursor: pointer;
  }
  & button:hover {
    background-color: #294c60;
    transform: scale(1.1);
  }

  & button svg {
    height: 26px;
    width: 26px;
    color: #fff;
  }
`
export const MenuItem = styled.div`
  display: flex;
  align-items: center;
  height: 42px;
  transition: all 0.2s;
  padding-left: 35px;
  padding-right: 35px;

  &:hover,
  &:focus {
    transform: scale(1.05);
    text-decoration: none;
    cursor: pointer;
    background-color: #294c60 !important;
  }

  & span {
    display: flex;
    justify-content: start;
    width: 50px;
  }

  & span svg {
    height: 20px;
    width: 20px;
    color: white;
    transition: all 0.25s;
  }

  &:hover > span svg {
    transform: scale(1.7);
  }

  & p {
    color: white;
    margin: 0;
    font-size: 16px;
    text-decoration: none;
  }
`
export const CardMenu = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 20px;

  & div img {
    object-fit: cover;
    margin-top: 20px;
    margin-bottom: 20px;
    border-radius: 50%;
    height: 100px;
  }

  & span {
    margin-top: 10px;
    color: white;
    font-weight: bold;
  }
`
