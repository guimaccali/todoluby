import React from 'react'

import { Node, Emoji } from './styles'

type InlineFace = {
  type: 'error' | 'warning' | 'success'
  message: string
  emoji?: string
}

function Inline({ type, message, emoji }: InlineFace) {
  return (
    <>
      <Node type={type}>
        {emoji ? <Emoji>{emoji}</Emoji> : ''}
        <span>{message}</span>
      </Node>
    </>
  )
}

export default Inline
