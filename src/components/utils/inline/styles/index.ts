import styled from 'styled-components'

const errorColor = '#9c0000'
const warningColor = '#9c8200'
const successColor = '#3c9c00'

export const Node = styled.span`
  font-size: '14px';
  font-weight: 'bold';
  color: ${(props: InlineStyleProps) =>
    `${
      props.type === 'success'
        ? successColor
        : props.type === 'warning'
        ? warningColor
        : props.type === 'error'
        ? errorColor
        : null
    }`};
`

export const Emoji = styled.span`
  margin-right: 5px;
`
