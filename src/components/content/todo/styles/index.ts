import styled from 'styled-components'

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin-top: 40px;
  margin-bottom: 40px;

  & textarea,
  & div {
    margin-right: 5px !important;
    margin-left: 5px !important;
  }
  & textarea {
    width: 100%;
    margin-bottom: 10px;
  }
`

export const Title = styled.h2`
  display: flex;
  justify-content: center;
  margin-top: 40px;
  margin-bottom: 0px;
`
