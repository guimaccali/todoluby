import React, { useState, useEffect } from 'react'

import Clickable from '../../utils/clickable'
import { FaPlus } from 'react-icons/fa'

import { Card, Title } from './styles'
import Input from '../../utils/input'

import * as yup from 'yup'
import YupHelper from '../../../helpers/YupHelper'
import TaskHelper from '../../../helpers/TaskHelper'

import TaskCard from '../../card/task'
import TaskContent from '../../content/task'
import Modal from '../../utils/modal'

const TaskSchema = yup.object().shape({
  description: yup
    .string()
    .min(5, 'The task must be at least 5 characters long')
})

function TodoContent() {
  const [taskList, setTaskList] = useState<Array<TaskFace>>([])
  const [modal, setModal] = useState<boolean>(false)
  const [currentTask, setCurrentTask] = useState<TaskFace | null>(null)
  const [description, setDescription] = useState<string>('')
  const [descriptionError, setDescriptionError] = useState<string>('')

  function errorVerify(errors: any) {
    errors.forEach((item: any) => {
      const { field, message } = item
      const firstLetter = field.charAt(0).toUpperCase()
      const restOfWord = field.slice(1)

      eval(`set${firstLetter}${restOfWord}Error('${message}')`)
    })
  }

  function sendTask() {
    TaskSchema.validate(
      {
        description
      },
      { abortEarly: false }
    )
      .then(data => {
        const task: TaskFace = data
        TaskHelper.record(task)
        setDescriptionError('')
        getTasks()
      })
      .catch(function (err) {
        const errors = YupHelper.errorTreatment(err)
        errorVerify(errors)
      })
  }

  function getTasks() {
    setTaskList(TaskHelper.getTasks())
  }

  function openModal(task: TaskFace) {
    // bodyControl(false)
    setCurrentTask(task)
    setModal(true)
    document.getElementById('scroll').scrollTop = 0
  }

  useEffect(() => {
    getTasks()
  }, [])

  return (
    <>
      <div className="container-fluid">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <Title>Write a Task</Title>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <Card>
                <Input
                  type="textarea"
                  name="description"
                  value={description}
                  onChange={event => {
                    setDescription(String(event.target.value))
                  }}
                  wrongMessage={descriptionError}
                />
                <Clickable
                  title="Add Task"
                  action={() => {
                    sendTask()
                  }}
                >
                  <FaPlus />
                  <span>Add</span>
                </Clickable>
              </Card>
            </div>
            {taskList.map((item: TaskFace, key) => (
              <div key={key} className="col-xs-12 col-md-4">
                <Clickable title="task" noStyle action={() => openModal(item)}>
                  <TaskCard
                    description={item.description}
                    dateTime={item.dateTime}
                  />
                </Clickable>
              </div>
            ))}
          </div>
        </div>
      </div>
      <Modal open={modal} setClose={() => setModal(!modal)}>
        {currentTask ? (
          <TaskContent
            description={currentTask.description}
            dateTime={currentTask.dateTime}
          />
        ) : (
          ''
        )}
      </Modal>
    </>
  )
}

export default TodoContent
