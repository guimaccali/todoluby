import styled from 'styled-components'

export const Date = styled.h2`
  display: flex;
  justify-content: center;
  margin-top: 30px;
`

export const Description = styled.p`
  &:first-letter {
    font-weight: bold;
    font-size: 18px;
    margin-right: 3px;
  }
`
