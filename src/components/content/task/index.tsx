import React from 'react'

import DateHelper from '../../../helpers/DateHelper'

import { Date, Description } from './styles'

function TaskCard({ description, dateTime }: TaskFace) {
  return (
    <>
      <div className="container-fluid">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <Date>{DateHelper.getDateTimeFormat(dateTime)}</Date>
            </div>
            <div className="col-xs-12">
              <Description>{description}</Description>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default TaskCard
