import React from 'react'

// External Libs
import Head from 'next/head'

// Page Content
import TodoContent from '../components/content/todo'

function Home() {
  return (
    <>
      <Head>
        <title>Todo Luby</title>
      </Head>
      <main>
        <TodoContent />
      </main>
    </>
  )
}

export default Home
